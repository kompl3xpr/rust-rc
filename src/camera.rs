use std::f64::consts::PI;

use crate::{
    config::CameraConfig,
    ray::Ray,
    util::random_in_unit_disk,
    vec3::{Point3, Vec3},
};

pub struct Camera {
    u: Vec3,
    v: Vec3,
    lens_radius: f64,
    origin: Point3,
    ll_corner: Point3,
    vertical: Vec3,
    horizontal: Vec3,
}

impl Camera {
    fn new(
        origin: Point3,
        dest: Point3,
        vfov: f64,
        vup: Vec3,
        aspect_ratio: f64,
        aperture: f64,
        focus_dist: f64,
    ) -> Self {
        let theta = vfov * PI / 180.0;
        let h = (theta / 2.0).tan();
        let viewport_height = 2.0 * h;
        let viewport_width = aspect_ratio * viewport_height;

        let w = (dest - origin).unit();
        let u = w.cross(vup).unit();
        let v = w.cross(u);

        let horizontal = focus_dist * viewport_width * u;
        let vertical = focus_dist * viewport_height * v;
        let ll_corner = origin + focus_dist * w - horizontal / 2.0 - vertical / 2.0;
        let lens_radius = aperture / 2.0;
        Self {
            origin,
            lens_radius,
            ll_corner,
            u,
            v,
            horizontal,
            vertical,
        }
    }

    pub fn from_config(cfg: &CameraConfig) -> Self {
        let origin = Point3::from_tuple(cfg.origin);
        let dest = Point3::from_tuple(cfg.dest);
        let vup = match cfg.vup {
            Some(vup) => Vec3::from_tuple(vup),
            None => Vec3(0.0, 1.0, 0.0),
        };
        let aspect_ratio = cfg.aspect_ratio.unwrap().0 / cfg.aspect_ratio.unwrap().1;
        let focus_dist = match cfg.focus_dist {
            Some(d) => d,
            None => (dest - origin).len(),
        };
        Self::new(
            origin,
            dest,
            cfg.vfov,
            vup,
            aspect_ratio,
            cfg.aperture,
            focus_dist,
        )
    }

    pub fn get_ray(&self, s: f64, t: f64) -> Ray {
        let rd = self.lens_radius * random_in_unit_disk();
        let offset = self.u * rd.0 + self.v * rd.1;
        Ray::new(
            self.origin + offset,
            self.ll_corner + s * self.horizontal + t * self.vertical - self.origin - offset,
        )
    }
}
