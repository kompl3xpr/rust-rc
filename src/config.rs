use serde::Deserialize;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

#[derive(Deserialize)]
pub struct Config {
    pub size: (u32, u32),
    pub sample_count: usize,
    pub save_path: String,
    pub calc_depth: usize,
    pub camera: CameraConfig,
}

#[derive(Deserialize)]
pub struct CameraConfig {
    pub origin: (f64, f64, f64),
    pub dest: (f64, f64, f64),
    pub vfov: f64,
    pub vup: Option<(f64, f64, f64)>,
    pub aspect_ratio: Option<(f64, f64)>,
    pub aperture: f64,
    pub focus_dist: Option<f64>,
}

impl Config {
    pub fn new(path: impl AsRef<Path>) -> Result<Self, ron::Error> {
        let reader = BufReader::new(File::open(path)?);
        let mut res: Self = ron::de::from_reader(reader)?;
        if res.camera.aspect_ratio.is_none() {
            res.camera.aspect_ratio = Some((res.size.0 as f64, res.size.1 as f64));
        }
        Ok(res)
    }
}
