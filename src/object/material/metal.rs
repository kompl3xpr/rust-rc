use crate::{
    object::{HitRecord, Material},
    ray::Ray,
    util::random_in_unit_sphere,
    vec3::Color,
};

pub struct Metal {
    albedo: Color,
    fuzz: f64,
}

impl Metal {
    pub fn new(albedo: Color, mut fuzz: f64) -> Self {
        fuzz = fuzz.clamp(0.0, 1.0);
        Self { albedo, fuzz }
    }
}

impl Material for Metal {
    fn scatter(&self, ray: &Ray, rec: &HitRecord) -> Option<(Ray, Color)> {
        let reflected = ray.direction.unit().reflect(rec.normal);
        let scattered = Ray::new(rec.point, reflected + self.fuzz * random_in_unit_sphere());
        match scattered.direction.dot(rec.normal) > -0.01 {
            true => Some((scattered, self.albedo)),
            false => None,
        }
    }
}
