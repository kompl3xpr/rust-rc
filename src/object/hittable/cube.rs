#![allow(unused)]

use crate::{object::{HitRecord, HitRecordBuilder, Hittable, Material}, ray::Ray, vec3::{Point3, Vec3}};

pub struct Cube {
    material: Box<dyn Material>,
}

impl Cube {
    pub fn new<M>(material: M) -> Self
    where
        M: Material + 'static,
    {
        todo!()
    }
}

impl Hittable for Cube {
    fn hit_by(&self, ray: &Ray) -> Option<HitRecord> {
        todo!()
    }
}
