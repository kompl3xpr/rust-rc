#![allow(unused)]

use crate::{object::{HitRecord, Hittable, Material}, ray::Ray, vec3::{Point3, Vec3}};

pub struct Rect {
    material: Box<dyn Material>,
}

impl Rect {
    pub fn new<M>(material: M) -> Self
    where
        M: Material + 'static,
    {
        todo!()
    }
}

impl Hittable for Rect {
    fn hit_by(&self, ray: &Ray) -> Option<HitRecord> {
        todo!()
    }
}