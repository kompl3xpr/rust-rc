use super::super::{HitRecord, HitRecordBuilder, Hittable};
use crate::{object::Material, ray::Ray, vec3::Point3};

pub struct Sphere {
    material: Box<dyn Material>,
    center: Point3,
    radius: f64,
}

impl Sphere {
    pub fn new<M>(center: Point3, radius: f64, material: M) -> Self
    where
        M: Material + 'static,
    {
        let material = Box::new(material);
        Self {
            center,
            radius,
            material,
        }
    }
}

impl Hittable for Sphere {
    fn hit_by(&self, ray: &Ray) -> Option<HitRecord> {
        let oc = ray.origin - self.center;
        let a = ray.direction.len_squared();
        let half_b = oc.dot(ray.direction);
        let c = oc.len_squared() - self.radius * self.radius;
        let discriminant = half_b * half_b - a * c;
        if discriminant > 0.0 {
            let t = (-half_b - discriminant.sqrt()) / a;
            let point = ray.at(t);
            let outward_normal = (point - self.center) / self.radius;
            Some(
                HitRecordBuilder::new(t, point, &*self.material)
                    .set_normal(ray, outward_normal)
                    .build(),
            )
        } else {
            None
        }
    }
}
