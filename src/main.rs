mod camera;
mod config;
mod object;
mod ray;
mod util;
mod vec3;
mod world;

use camera::*;
use config::*;
use image::{ImageFormat, RgbImage};
use object::{
    hittable::Sphere,
    material::{Dielectric, Diffuse, Metal},
};
use rand::random;
use util::*;
use vec3::*;
use world::World;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let Config {
        size: (width, height),
        sample_count,
        save_path,
        calc_depth,
        camera,
    } = Config::new("./config.ron")?;

    let mut image = RgbImage::new(width, height);
    let camera = Camera::from_config(&camera);
    let world = world_of_example_1(calc_depth);

    for j in (0..height).rev() {
        for i in 0..width {
            let mut color = Color::new(0.0, 0.0, 0.0);
            for _ in 0..sample_count {
                let u = (i as f64 + random::<f64>()) / (width - 1) as f64;
                let v = (j as f64 + random::<f64>()) / (height - 1) as f64;
                let ray = camera.get_ray(u, v);
                color += world.get_color(&ray);
            }
            color /= sample_count as f64;
            image.put_pixel(i, height - 1 - j, pixel_from(color));
        }
    }
    image.save_with_format(&save_path, ImageFormat::Jpeg)?;
    Ok(())
}

fn world_of_example_1(calc_depth: usize) -> World {
    World::new(calc_depth)
        .add(Sphere::new(
            Point3::new(12.0, 1.5, -4.0),
            2.0,
            Diffuse::new(Color::new(0.2, 0.2, 0.5)),
        ))
        .add(Sphere::new(
            Point3::new(10.0, 0.0, -1.0),
            0.5,
            Dielectric::new(1.2),
        ))
        .add(Sphere::new(
            Point3::new(8.8, 0.5, -2.5),
            1.0,
            Metal::new(Color::new(1.0, 0.55, 0.1), 0.01),
        ))
        .add(Sphere::new(
            Point3::new(9.6, -0.35, -0.6),
            0.15,
            Metal::new(Color::new(0.7, 0.7, 1.0), 0.1),
        ))
        .add(Sphere::new(
            Point3::new(10.8, 0.1, 0.0),
            0.6,
            Diffuse::new(Color::new(0.7, 0.05, 0.05)),
        ))
        .add(Sphere::new(
            Point3::new(10.0, -1000.5, -1.0),
            1000.0,
            Metal::new(Color::new(0.7, 0.7, 0.7), 0.4),
        ))
        .add(Sphere::new(
            Point3::new(8.6, -0.15, -1.2),
            0.35,
            Dielectric::new(2.4),
        ))
}
